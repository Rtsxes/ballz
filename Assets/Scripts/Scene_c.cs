﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Scene_c : MonoBehaviour
{
    public Image tierFieldImage;
    public Image availabelBallFill;


    public int completedBallsCount;
    public int availabelBallCount;
    public int winBallsCount;


    [System.Serializable]
    public class Point
    {
    public List<GameObject> level;
    }
 
    [System.Serializable]
    public class PointList
    {
    public List<Point> tier;
    }

    public PointList units;
    public GameObject nowUnit;
    public GameObject ballSpawner;
    public GameObject money_c;
    public Text tierText;
    public Text completedBallsCountText;
    public List<int> ballsCountToChangeTier;
    public int  tierBallCount;
    public int currentTier;
    public int existBallsCount;
    int r;
    bool delay;
    public GameObject losePanel;
    public int ad;

    // Start is called before the first frame update
    void Start()
    {
        currentTier = 0;
        RefreshText();
        GetNewUnit();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void AddWinBall()
    {
        completedBallsCount++;
        if(completedBallsCount == winBallsCount)
        {
            Debug.Log("get bonus");
        }
        RefreshText();
        AddBallToTier();
    }


    public void AddBallToTier()
    {
        if(currentTier < ballsCountToChangeTier.Count)
        {
            tierBallCount++;
            tierFieldImage.fillAmount = (float)tierBallCount / (float)ballsCountToChangeTier[currentTier];
            //change sprite
            if (tierBallCount == ballsCountToChangeTier[currentTier])
            {
                if(currentTier < units.tier.Count-1)
                {
                    ChangeTier();
                }
            }
        }
    }


    public void ChangeTier()
    { 
        currentTier++;
        tierBallCount = 0;
        //animation
        tierText.text ="Текущий тир: " + currentTier.ToString();
    }


    public void CheckUnit()
    {
        if(completedBallsCount > 0 && existBallsCount == 0 && availabelBallCount == 0)
        {
            money_c.GetComponent<Money_c>().ChangeMoney(completedBallsCount);
            nowUnit.GetComponent<UnitInfo>().Complete();
            GetNewUnit();
            StartCoroutine(fillBallsCount());
        }
        if(completedBallsCount == 0 && existBallsCount == 0 && availabelBallCount == 0)
        {
            // Lose
           // SceneManager.LoadScene(0);
            losePanel.GetComponent<Lose>().LoseGame();
        }
    }


    IEnumerator fillBallsCount()
    {

        while(completedBallsCount != 0)
        {
            completedBallsCount--;
            availabelBallCount++;
            ballSpawner.GetComponent<BallSpawner>().ballCount = availabelBallCount;
            RefreshText();
            yield return null;
        }
        ReturnBalls(0);
    }

    public void GetNewUnit()
    {   
        r = Random.Range(0,units.tier[currentTier].level.Count);
        nowUnit = Instantiate(units.tier[currentTier].level[r],new Vector3(0,6,0),Quaternion.identity);
    }


    public void ReturnBalls(int ad)
    {
        if (ad == 0)
        {
            ballSpawner.GetComponent<BallSpawner>().ballCount = availabelBallCount;
            //availabelBallCount = completedBallsCount;
            winBallsCount = availabelBallCount;
            //completedBallsCount = 0;
            ballSpawner.GetComponent<BallSpawner>().SetBallText();
            RefreshText();
        }
        if (ad == 1)
        {
            ballSpawner.GetComponent<BallSpawner>().ballCount = 5;
            availabelBallCount = 5;
            winBallsCount = 5;
            //completedBallsCount = 0;
            ballSpawner.GetComponent<BallSpawner>().SetBallText();
            RefreshText();
        }
    }


    public void RefreshText()
    {
        tierText.text = "Текущий тир: " + (currentTier + 1).ToString();
        completedBallsCountText.text = completedBallsCount.ToString();
        ballSpawner.GetComponent<BallSpawner>().SetBallText();
    }


    public void RefreshFill()
    {
       /*  availabelBallFill.fillAmount =  (float)availabelBallCount / (float)winBallsCount;
        completedBallsFill.fillAmount = (float)completedBallsCount / (float)winBallsCount;*/
    }

    public void Tomenu()
    {
        SceneManager.LoadScene(0);
    }

}
