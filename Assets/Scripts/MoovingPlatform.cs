﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoovingPlatform : MonoBehaviour
{
    public GameObject endPosition;
    public Vector3 strartPosition;
    public Vector3 endPositionVector;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        strartPosition = transform.position;
        endPositionVector = endPosition.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x > endPositionVector.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, endPosition.transform.position, speed);
        }
        if(transform.position.x < endPositionVector.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, strartPosition, -speed);
        }
    }
}
