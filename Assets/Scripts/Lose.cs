﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Lose : MonoBehaviour
{
    public GameObject ressurectButton;
    public GameObject restartButton;
    public GameObject ballSpawner;
    public int addedBalls;
    public GameObject scene_c;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
        ressurectButton.SetActive(false);
        restartButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoseGame()
    {
        gameObject.SetActive(true);
    }


    public void OnEnable()
    {
        StartCoroutine(delay(2));
    }  

    private IEnumerator delay(float duration)
    {
        float normalizedTime = 0;
        while(normalizedTime <= 1f)
        {
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }
        ressurectButton.SetActive(true);
        restartButton.SetActive(true);
    }


    public void Restart()
    {
        SceneManager.LoadScene(1);
    }


    public void Ressurection()
    {
        scene_c.GetComponent<Scene_c>().ReturnBalls(1);
        gameObject.SetActive(false);
    }



}
