﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallInfo : MonoBehaviour
{
    public float ballSpeed;
    public float rotationSpeed;
    public GameObject scene_c;
    public Scene_c sceneControler;

    // Start is called before the first frame update
    void Start()
    {

        scene_c = GameObject.FindGameObjectWithTag("scene_c");
       // transform.GetComponent<Rigidbody2D>().velocity = new Vector3(0, ballSpeed, 0);
        //Destroy(gameObject,5);
        sceneControler = scene_c.GetComponent<Scene_c>();
        sceneControler.availabelBallCount--;
        sceneControler.existBallsCount++;
        sceneControler.RefreshFill();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0,0,Time.deltaTime*rotationSpeed);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        //сталкивание с выйгрышной чертой
        if(other.tag == "winBox")
        {
            sceneControler.existBallsCount--;
            sceneControler.AddWinBall();
            sceneControler.CheckUnit();
            sceneControler.RefreshFill();
            Destroy(gameObject);
        }
        //сталкивание с препятствием
        if(other.tag == "wall")
        {
            sceneControler.existBallsCount--;
            sceneControler.CheckUnit();
            sceneControler.RefreshFill();
            Destroy(gameObject);
        }
    }
}
