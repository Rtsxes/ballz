﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BallSpawner : MonoBehaviour, IDragHandler, IBeginDragHandler, IPointerDownHandler,IPointerUpHandler
{
    public GameObject ballSpawner;
    bool isFire;
    public GameObject ballPb;
    GameObject ballObj;
    public float fireRate;
    float fireTime;
    public float nonMagikalY;
    public int ballCount;
    public GameObject scene_c;
    public Text ballCountText;
    public List<Sprite> skins;
    public int currentSkinNum;
    public bool isDecrease;
    public bool isSpeedUp;
    public float ballSpeed;

    // Start is called before the first frame update
    void Start()
    {
        currentSkinNum = PlayerPrefs.GetInt("currentSkinNum");
        scene_c = GameObject.FindGameObjectWithTag("scene_c");
        isFire = false;
        ballCountText.text = ballCount.ToString();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        fireTime += Time.deltaTime;
        if(isFire && fireTime >= fireRate && ballCount > 0)
        {
            SpawnNewBalls();
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    { 

    }

    // таскаем спавнер
    public void OnDrag(PointerEventData eventData)      
    { 
	    ballSpawner.transform.position = eventData.pointerCurrentRaycast.worldPosition;
        isFire = true;

    }


    public void OnPointerDown(PointerEventData eventData)
    {
        ballSpawner.transform.position = eventData.pointerCurrentRaycast.worldPosition;
        isFire = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isFire = false;
    }


    public void SpawnNewBalls()
    {
        fireTime = 0;
        ballObj = Instantiate(ballPb, new Vector3 (ballSpawner.transform.position.x, nonMagikalY, 0), ballSpawner.transform.rotation);
        ballObj.GetComponent<SpriteRenderer>().sprite = skins[currentSkinNum];
        ballObj.transform.GetComponent<Rigidbody2D>().velocity = new Vector3(0, ballSpeed, 0);
        ballCount--;
        if(isDecrease)
        {
            DecreaseBalls(ballObj);
        }
        if(isSpeedUp)
        {
            ballObj.transform.GetComponent<Rigidbody2D>().velocity = new Vector3(0, ballSpeed*2, 0);
        }
        SetBallText();
    }


    public void SetBallText()
    {
        ballCountText.text = ballCount.ToString();
    }


    public void DecreaseBalls(GameObject ball)
    {
        ball.transform.localScale /= 2;
    }

    public void StartDecrease()
    {
        isDecrease = true;
    }


    public void EndBonuses()
    {
        isDecrease = false;
        isSpeedUp = false;
    }

    public void StartSpeedUp()
    {
        isSpeedUp = true;
    }
}
