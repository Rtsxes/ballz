﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Shop_C : MonoBehaviour
{

    public GameObject ballPb;
//наборы элементов с меню и магазином
    public GameObject shop;
    public GameObject menu;
    // список скинов
    public List<GameObject> skins;
    //текущий выбраннй скин
    public Sprite currentSkin;
    // номер выбранного скина
    public int currentSkinNum;
    //стартовое время задержки появления обводки
    //для крутого эффекта лутбокса
    public float startDelay;
    //изменение скорости для того же эффекта
    public float deltaSpeed;
    //переменная для рандома
    public int r;
    //общее количество прыжков
    public int jumpsCount;
    //количество совершённых прыжков
    //это можно убрать из паблика но для тестов пусть висит
    public int currentJump;
    //для тестов
    public Text rewardText;
    public bool isRoulette;
    public GameObject startButton;

    //тут всё связанное с деньгами
    //хз для чего он тут 
    public GameObject money_c;
    public Text moneyText;
    public int money;
    public int skinCost;

    public GameObject avaliableSkinText;

    // Start is called before the first frame update
    void Start()
    {
        money = PlayerPrefs.GetInt("money");
        currentSkinNum = PlayerPrefs.GetInt("currentSkinNum");
        skins[currentSkinNum].transform.GetChild(0).gameObject.SetActive(true);
        RefreshSkinAvaliable();
        RefreshMoneyText();
        /*  
        if(money >= skinCost)
        {
            avaliableSkinText.SetActive(true);
        }
        */
    }

    // Update is called once per frame
    void Update()
    {
        if(money >= skinCost && avaliableSkinText.activeSelf == false)
        {
            avaliableSkinText.SetActive(true);
        }
        if(money <= skinCost && avaliableSkinText.activeSelf == true)
        {
            avaliableSkinText.SetActive(false);
        }
    }

    //начинаем рулетку
    public void StartRoulette()
    {
        if(money >= skinCost)
        {
            if(currentJump < jumpsCount)
            {
            startButton.gameObject.SetActive(false);
            isRoulette = true;
            r = Random.Range(0,skins.Count);
            //рандомим пока не выпадет номер закрытого элемента
            //те что уже открыты мы игнорируем
            while(PlayerPrefs.GetInt("skin"+r) == 1)
            {
                r = Random.Range(0,skins.Count);
            }
            //включаем кружок вокруг скина
            skins[r].transform.GetChild(0).gameObject.SetActive(true);
            //корунтин с задержкой кружка на экране
            StartCoroutine(delay(startDelay));
            currentJump++;
            }
        }
    }

    private IEnumerator delay(float duration)
    {

        float normalizedTime = 0;
        while(normalizedTime <= 1f)
        {
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }
        if(currentJump < jumpsCount)
        {
            skins[r].transform.GetChild(0).gameObject.SetActive(false);
            //крутим рулетку пока не совершим все прыжки
            StartRoulette();
        }
        else
        {
            skins[r].transform.GetChild(0).gameObject.SetActive(false);
            currentJump = 0;
            PlayerPrefs.SetInt("skin"+r,1);
            //когда совершили все прыжки то открываем скин
            RefreshSkinAvaliable();
            isRoulette = false;
            startButton.gameObject.SetActive(true);
            // изменяем количество денег и обновляет надпись на экране
            money_c.GetComponent<Money_c>().ChangeMoney(-skinCost);
            RefreshMoneyText();
        }
    }


    public void RefreshMoneyText()
    {
        money = PlayerPrefs.GetInt("money");
        moneyText.text = money.ToString() + "/" + skinCost;
    }

    //просто обновление состояния скинов открыт или закрыт
    public void RefreshSkinAvaliable()
    {
       for (int i = 1;i < skins.Count;i++)
       {
           if(PlayerPrefs.GetInt("skin"+i) == 0)
           {
               skins[i].GetComponent<Image>().color = new Color32(255,255,255,50);
           }
           else
           {
               skins[i].GetComponent<Image>().color = new Color32(255,255,255,255);
           }
       }
    } 

    //показ меню магазина
    public void ShowShop()
    {
        menu.SetActive(false);
        shop.SetActive(true);
        RefreshMoneyText();
        RefreshSkinAvaliable();
    }

    public void CloseShop()
    {
        menu.SetActive(true);
        shop.SetActive(false);
    }

    // клацание по скину висит на каждом скине
    // и передаёт свой номер
    public void ChooseSkin(int skinNum)
    {  
        if(!isRoulette)
        {
            if(PlayerPrefs.GetInt("skin"+skinNum) == 1) 
            {
        // не придумал ничего лучше нежели это
            for (int i = 0;i < skins.Count;i++)
            {
                skins[i].transform.GetChild(0).gameObject.SetActive(false);
            }
            skins[skinNum].transform.GetChild(0).gameObject.SetActive(true);
            PlayerPrefs.SetInt("currentSkinNum",skinNum);
            }
        }
    }


    public void GiveMeMoreMoney()
    {
        money_c.GetComponent<Money_c>().ChangeMoney(500);
    }

    public void RefreshSkins()
    {
        for (int i = 1;i < skins.Count;i++)
       {
            PlayerPrefs.SetInt("skin"+i,0);
       }
       RefreshSkinAvaliable();
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
}
