﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class AdController : MonoBehaviour
{
    private RewardBasedVideoAd rewardBasedVideo;
    public GameObject money_c;
    public Text text1;
    // Start is called before the first frame update
    void Start()
    {
        rewardBasedVideo = RewardBasedVideoAd.Instance;
        // Called when an ad request has successfully loaded.
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
        LoadRewardVideoAd();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadRewardVideoAd()
    {
        rewardBasedVideo.LoadAd(new AdRequest.Builder().Build(),"ca-app-pub-3940256099942544/5224354917");
    }

    public void ShowRewardVideoAd()
    {
        if(rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.Show();
            text1.text = "load";
        }
        else
        {
            text1.text = "notload";
        }
    }

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {

    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {

    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {

    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {

    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args) 
    {
        //reward player
        money_c.GetComponent<Money_c>().ChangeMoney(10000);
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {

    }

}
