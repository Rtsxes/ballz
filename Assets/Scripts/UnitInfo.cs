﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitInfo : MonoBehaviour
{
    public bool complete;
    public List<GameObject> items;
    public GameObject ballSpawner;

    void Start()
    {
        ballSpawner = GameObject.FindGameObjectWithTag("ballSpawner");
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if(complete)
        {
            gameObject.transform.position = Vector3.MoveTowards(transform.position, transform.position - new Vector3(0, 6, 0),Time.deltaTime*6);
        }
        else
        {
            gameObject.transform.position = Vector3.MoveTowards(transform.position, new Vector3(0, 0, 0), Time.deltaTime * 6);
        }
    }
    public void Complete()
    {
        for(int i = 0; i < items.Count; i++)
        {
            items[i].GetComponent<Collider2D>().enabled = false;
        }
        ballSpawner.GetComponent<BallSpawner>().EndBonuses();
        Destroy(gameObject, 3);
        complete = true;
    }
}
