﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Money_c : MonoBehaviour
{
    public int money;
    public Text moneyText;
    
    // Start is called before the first frame update
    void Start()
    {
        money = PlayerPrefs.GetInt("money");
        moneyText.text = money.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeMoney(int count)
    {
        money += count;
        if(moneyText != null)
        {
            moneyText.text = money.ToString();
        }
        PlayerPrefs.SetInt("money",money);
    }
}
